#include <iostream>
#include <functional>
#include <string>

auto calculate (int value, int min, int max, std::function<int(int, int)> operation) -> int
{
  for (; min < max; ++min)
    value = operation(value, min);

  return value;
}

auto calculate_sum(int min, int max) -> int {

  auto sum_operation = [](int result, int value) {
    return result + (value % 2 ? value : 0);
  };

  return calculate(0, min, max, sum_operation);
}

auto calculate_san (int min, int max) -> int {

  auto san_operation = [](int result, int value) {
    return result * (value % 2 ? 1 : value);
  };

  return calculate(1, min, max, san_operation);
}

auto get_int (std::string message) -> int {
  std::cout << message;
  int n; std::cin >> n;
  return n;
}

int main()
{
  auto min{get_int("Minimum: ")};
  auto max{get_int("Maximum: ")};

  auto sum{calculate_sum(min, max)};
  auto san{calculate_san(min, max)};

  std::cout
    << "Sum of all odd numbers in range: " << sum << '\n'
    << "Product of all even number in range: " << san << '\n'
  ;
  return 0;
}
